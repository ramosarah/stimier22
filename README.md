# Environnement LAMP basé sur Docker Compose

## Usage

Pour récupérer le projet, créez un dossier, allez dedans et faites:
```
$ git init
$ git clone https://gitlab.com/ramosarah/stimier22

```

Pour démarrer l’environnement:

```
$ docker-compose up -d
```

Puis, se rendre dans votre navigateur et taper:
```
localhost:8080
```
