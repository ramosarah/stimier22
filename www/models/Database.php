<?php
require("Contact.php");
class Database{
  private $connexion;
  public function getConnexion() {return $this->connexion;}

  const DB_HOST = "mariadb";
  const DB_PORT = "3306";
  const DB_NAME = "contacts";
  const DB_USER = "root";
  const DB_PASSWORD = "digital2019";
  public function __construct(){
    try{
      $this->connexion = new PDO(
        "mysql:host=".self::DB_HOST.";port=".self::DB_PORT.";dbname=".self::DB_NAME.";charset=UTF8",self::DB_USER,self::DB_PASSWORD
      );

    }catch(PDOException $e){
      echo 'Connexion échouée : '.$e->getMessage();
    }
  }



  public function createContact($name, $email, $message){
    $pdoStm = $this->connexion->prepare(
      "INSERT INTO contacts(name, email, message) VALUES (:name, :email, :message)"
    );
    $pdoStm->execute([
      "name"      =>      $name,  
      "email"     =>      $email,
      "message"   =>      $message
    ]);


    $id = $this->connexion->lastInsertId();
    return $id;
  }

}
?>